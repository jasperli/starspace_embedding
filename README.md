# starspace_embedding

Implementation of StarSpace by Facebook AI with Tensorflow eagar-mode.

# reference

1. [github - StarSpace](https://github.com/facebookresearch/StarSpace)
2. [arxiv - StarSpace: Embed All The Things!](https://arxiv.org/abs/1709.03856)