import numpy as np
import tensorflow as tf
tf.enable_eager_execution()


def inner_product(a, b):
    '''
    inner_prooduct per row 
    
    args:
    :a: tf.tensor with shape = (num_samples, k, emb_size)
    :b: tf.tensor with shape = (num_samples, k, emb_size)
    
    returns:
    :c: tf.tensor with shape = (num_samples, k)
    '''
    c = tf.reduce_sum(tf.multiply(a, b), axis=1)
    return c


def cosine_similarity(a, b):
    
    c = tf.reduce_sum(tf.multiply(a, b), axis=1) # shape = (num_samples, 1)
    a_norm = tf.norm(a, ord="euclidean", axis=1) # shape = (num_samples, 1)
    b_norm = tf.norm(b, ord="euclidean", axis=1) # shape = (num_samples, k)
    divisor = tf.sqrt(a_norm) * tf.sqrt(b_norm)
    c = tf.divide(c, divisor)
    return c


class StarSpace(object):
    def __init__(self, 
                 vocab_size, 
                 embedding_size, 
                 hashtable, 
                 similarity="cosine", 
                 loss="mrl", 
                 margin=0.1,
                 optimizer="adam",
                 optimizer_params={},
                 batch_size=256,
                 print_batch=False):
        """
        Desc:
        
        Params:
        :vocab_dict: dict.
        :embedding_size: int.
        :similarity: str.
        :loss_type: str.
        
        Return:
        
        """
        self.vocab_size = vocab_size
        self.embedding_size = embedding_size
        self.hashtable = hashtable
        
        self.similarity = similarity
        self.loss_type = loss
        self.margin = margin
        
        self.optimizer_type = optimizer
        self.optimizer_params = optimizer_params
        self.batch_size = batch_size
        
        self.global_step = tf.Variable(0, trainable=False, name="global_step")
        
        self.weights = self._init_weights()
        self.optimizer = self._init_optimizer()
        
        self.print_batch = print_batch
    
    
    def _init_weights(self):
        """
        Desc:
        
        Params:
        
        Return:
        
        """
        weights = dict()
        with tf.name_scope("weights"):
            weights["emb_table"] = tf.get_variable(
                "embedding_table",
                [self.vocab_size, self.embedding_size],
                initializer=tf.truncated_normal_initializer(0.0, 1.0/np.sqrt(self.embedding_size))
            )
        return weights
    
    
    def _init_optimizer(self):
        """
        Desc:
        
        Params:
        
        Return:
        
        """
        if isinstance(self.optimizer_type, str):
            if self.optimizer_type is "adam":
                return tf.train.AdamOptimizer(**self.optimizer_params)
            elif self.optimizer_type is "adagrad":
                return tf.train.AdagradOptimizer(**self.optimizer_params)
            elif self.optimizer_type is "momentum":
                return tf.train.MomentumOptimizer(**self.optimizer_params)
            elif self.optimizer_type is "rmsprop":
                return tf.train.RMSPropOptimizer(**self.optimizer_params)
            elif self.optimizer_type is "sgd":
                return tf.train.GradientDescentOptimizer(**self.optimizer_params)
        else:
            raise NotImplementedError()
    
    
    def _compute_loss(self, 
                      target_inputs, 
                      context_inputs, 
                      negative_inputs):
        """
        Desc:
        
        Params:
        
        Return:
        
        """
        # compute the loss of inputs
        # convert inputs to ragged constant
        target_inputs = tf.ragged.constant(target_inputs)      # shape=(None, )
        context_inputs = tf.ragged.constant(context_inputs)    # shape=(None, None)
        negative_inputs = tf.ragged.constant(negative_inputs)  # shape=(None, None)
        
        # lookup index values with keys
        target_inputs = tf.ragged.map_flat_values(
            lambda x: self.hashtable.lookup(x), target_inputs)   # shape=(None, )
        context_inputs = tf.ragged.map_flat_values(
            lambda x: self.hashtable.lookup(x), context_inputs)  # shape=(None, None)
        negative_inputs = tf.ragged.map_flat_values(
            lambda x: self.hashtable.lookup(x), negative_inputs) # shape=(None, None)
        
        # lookup embedding vectors with index values
        target_emb = tf.ragged.map_flat_values(
            tf.nn.embedding_lookup, self.weights["emb_table"], target_inputs)   # shape=(None, emb_size)
        context_emb = tf.ragged.map_flat_values(
            tf.nn.embedding_lookup, self.weights["emb_table"], context_inputs)  # shape=(None, ?, emb_size)
        negative_emb = tf.ragged.map_flat_values(
            tf.nn.embedding_lookup, self.weights["emb_table"], negative_inputs) # shape=(None, ?, emb_size)
        
        # calculate mean per row in context_emb and negative_emb
        context_emb_mean = tf.reduce_mean(context_emb, axis=1)
        negative_emb_mean = tf.reduce_mean(negative_emb, axis=1)
        
        # need to calculate the similarity between one negative entity and target entity 
        # rather than the mean of negative entities. TO BE UPDATED!!
        if self.similarity == "dot":
            pos_sim = inner_product(target_emb, context_emb_mean)
            neg_sim = inner_product(target_emb, negative_emb_mean)
        elif self.similarity == "cosine":
            pos_sim = cosine_similarity(target_emb, context_emb_mean)
            neg_sim = cosine_similarity(target_emb, negative_emb_mean)
        
        if self.loss_type == "mrl":
            """margin ranking loss"""
            diff = self.margin + neg_sim - pos_sim
            loss = tf.nn.relu(diff)
        elif self.loss_type == "softmax":
            sim_concat = tf.concat([pos_sim, neg_sim], axis=1)
            softmax = tf.nn.softmax(sim_concat, axis=1)
            first_column = tf.slice(softmax, [0, 0], [target_inputs.get_sahpe().aslist()[0], 1])
            loss = -tf.log(first_column)
        loss = tf.reduce_mean(loss)
        return loss
    
    
    def _compute_grads(self, 
                       target_inputs, 
                       context_inputs, 
                       negative_inputs):
        """
        Desc:
        
        Params:
        
        Return:
        
        """
        # compute the gradient of inputs
        with tf.GradientTape() as tape:
            loss = self._compute_loss(target_inputs,
                                      context_inputs,
                                      negative_inputs)
        grads = tape.gradient(loss, list(self.weights.values()))
        return grads
    
    
    def _fit_on_batch(self, 
                      target_inputs, 
                      context_inputs, 
                      negative_inputs):
        """
        Desc:
        
        Params:
        
        Return:
        
        """
        grads = self._compute_grads(target_inputs,
                                    context_inputs,
                                    negative_inputs)
        
        self.optimizer.apply_gradients(zip(grads, list(self.weights.values())),
                                       global_step=tf.train.get_or_create_global_step())
    
    
    def _fit_per_epoch(self, 
                       target_inputs, 
                       context_inputs, 
                       negative_inputs):
        """
        Desc:
        
        Params:
        
        Return:
        
        """
        # create a variable to save the batch step inside epochs
        batch_step = tf.Variable(1, trainable=False, name="batch_step")
        
        # get total steps of training
        len_inputs = len(target_inputs)
        total_steps = len_inputs // self.batch_size
        total_steps = total_steps + 1 if total_steps / self.batch_size == 0 else total_steps
        
        # get print steps size
        print_steps = int(total_steps * 0.2)
        
        # convert list to np.array to index
        target_inputs = np.array(target_inputs)
        context_inputs = np.array(context_inputs)
        negative_inputs = np.array(negative_inputs)
        
        # create shuffle indices of the inputs
        shuffle_indices = np.random.permutation(len_inputs)
        
        _loss = []
        for i in range(total_steps):
            shuffle_index = shuffle_indices[self.batch_size * i:self.batch_size * (i + 1)]
            self._fit_on_batch(list(target_inputs[shuffle_index]),
                               list(context_inputs[shuffle_index]),
                               list(negative_inputs[shuffle_index]))
            if (i + 1) % print_steps == 0:
                loss = self._compute_loss(list(target_inputs[shuffle_index]),
                                          list(context_inputs[shuffle_index]),
                                          list(negative_inputs[shuffle_index]))
                _loss.append(loss)
                
                if self.print_batch:
                    print(f"Loss at batch step {batch_step.numpy()}: {loss}")
            
            self.global_step = self.global_step + 1
            batch_step = batch_step + 1
        
        loss = self._compute_loss(list(target_inputs[shuffle_index]),
                                  list(context_inputs[shuffle_index]),
                                  list(negative_inputs[shuffle_index]))
        _loss.append(loss)
        return _loss
    
    def fit(self, 
            target_inputs, 
            context_inputs, 
            negative_inputs,
            epochs=10):
        """
        Desc:
        
        Params:
        
        Return:
        
        """
        history = []
        epochs_t = tf.Variable(1, trainable=False, name="epochs")
        
        for i in range(epochs):
            print(f"Epochs: {epochs_t.numpy()}/{epochs}:")
            loss_per_batch = self._fit_per_epoch(target_inputs, 
                                                 context_inputs, 
                                                 negative_inputs)
            avg_loss = np.mean(loss_per_batch)
            history.append(avg_loss)
            print(f"Avg Loss per epochs {epochs_t.numpy()}: {avg_loss}")
            
            epochs_t = epochs_t + 1
        
        return history
    
    
    def save_params(self, save_path="./saved_params/"):
        """
        Desc:
        
        Params:
        
        Return:
        
        """
        for k, v in self.weights.items():
            np.save(save_path + k + ".npy", v.numpy())
        